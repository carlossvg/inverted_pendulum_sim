clear all, close all, clc

m = 1;
M = 5;
L = 2;
g = -10;
d = 20;

data = dlmread("../simulations/sim_data.txt"," ");
t = data(:,1);
y = data(:,2:5);
f = data(:,6);

h = figure;
for k=1:length(y)
    drawcartpend(y(k,:),m,M,L);
end
