clear all, close all, clc

data = dlmread("../simulations/sim_data.txt"," ");

m = 1;
M = 5;
L = 2;
g = -10;
d = 20;

step = 0.1;

t = data(:,1);
y = data(:,2:5);
f = data(:,6);

figure;
subplot(2,2,1)
plot(t,y(:,1))
title('cart position')
xlabel('time (s)')
ylabel('m');

subplot(2,2,2)
plot(t,y(:,3))
title('pendulum angle')
xlabel('time (s)')
ylabel('rad');

subplot(2,2,3)
plot(t,y(:,2))
title('cart velocity')
xlabel('time (s)')
ylabel('m/s');

subplot(2,2,4)
plot(t,y(:,4))
title('pendulum angular velocity')
xlabel('time (s)')
ylabel('rad/s');

figure;
plot(t,f)
ylabel('N')
xlabel('time (s)')
title('cart force');