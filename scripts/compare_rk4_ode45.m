clear all, close all, clc

data = dlmread("../simulations/sim_data.txt"," ");

m = 1;
M = 5;
L = 2;
g = -10;
d = 20;

step = 0.1;

t1 = data(:,1);
y1 = data(:,2:5);

tspan = 0:.1:50;
y0 = [0; 0; pi; -.5];

[t2,y2] = ode45(@(t,y2)cartpend(y2,m,M,L,g,d,0),tspan,y0);

figure(1);

subplot(2,2,1)
hold on;
plot(t1,y1(:,1))
plot(t2,y2(:,1))
ylabel('m')
xlabel('t')
title('cart position')
legend('rk4', 'ode45')
hold off;

subplot(2,2,2)
hold on;
plot(t1,y1(:,2))
plot(t2,y2(:,2))
ylabel('m/s')
xlabel('t')
title('cart velocity')
legend('rk4', 'ode45')
hold off;

subplot(2,2,3)
hold on;
plot(t1,y1(:,3))
plot(t2,y2(:,3))
ylabel('rad/s')
xlabel('t')
title('pendulum angle')
legend('rk4', 'ode45')
hold off;

subplot(2,2,4)
hold on;
plot(t1,y1(:,4))
plot(t2,y2(:,4))
ylabel('rad')
xlabel('t')
title('pendulum velocity')
legend('rk4', 'ode45')
hold off;
