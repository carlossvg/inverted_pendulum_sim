# Inverted pendulum simulation in C++


This project aims to test the simulation of a inverted pendulum in C++.


## Compilation

To compile the simulation program:

```
g++ sim_pendulum.cpp  -o sim_pendulum -I include
```

## Runge kutta integrator

To solve the inverted pendulum differential equations we use the Runge-Kutta method. The used code is an adaptation a C code I used a while ago in a University practice. I used a the 4th order classical method which uses a trapezoidal rule. We compare the result simulating the pendulum with the octave ode45 solver and the both simulations seem to match.

<img src="./images/compare_rk4_ode45.png">


## Octave Scripts


We simulation program writes the simulation data as csv values in a file. We use octave or Matlab to plot and animate the simulation. I'm using as a base the scripts provided by Steve Burton in his [Control Bootcamp series](https://www.youtube.com/playlist?list=PLMrJAkhIeNNR20Mz-VpzgfQs5zrYi085m).

To run the the inverted pendulum animation we run the script `animate_pendulum.m`. To show the plots we use the script  `plot_simulatiom.m`.


## Examples

### No controller

To simulate the pendulum with no controller run:

```
./sim_pendulum --pend-pos 3.14159 --pend-vel -0.00 -n 500
```

![](./images/no_controller.gif)


<img src="./images/no_controller.png">



### No controller. Impulse response


To simulate the pendulum with no controller and with a external impulse type force run:

```
./sim_pendulum --pend-pos 0.0 -n 200 --signal-type i --t0 50 --force-amplitude 100
```


![](./images/impulse.gif)

<img src="./images/impulse_force.png" width="600">

<img src="./images/impulse.png">


### No controller. Step response

To simulate the pendulum with no controller and with a external step type force run:

```
./sim_pendulum --pend-pos 0.0 -n 200 --signal-type s --t0 50 --force-amplitude 10
```

![](./images/step.gif)

<img src="./images/step_force.png" width="600">

<img src="./images/step.png">


### No controller. Ramp response

Now we simulate an external ramp type force disturbance run:

```
./sim_pendulum --pend-pos 0.0 -n 200 --signal-type r --t0 10 --t1 200 --force-amplitude 10
```

![](./images/ramp.gif)

<img src="./images/ramp_force.png" width="600">

<img src="./images/ramp.png">


### No controller. Square response

Now we simulate an external quare wave type force disturbance run:


```
./sim_pendulum --pend-pos 0.0 -n 150 --signal-type q --t0 40 --t1 60 --force-amplitude 10
```

![](./images/square.gif)

<img src="./images/square_force.png" width="600">

<img src="./images/square.png">


### Controller

Now we use a full state feedback controller to control the inverted pendulum. By default the feedback matrix K gains are tuned for the default pendulum model parameters.

```
./sim_pendulum --pend-vel -0.5 -n 200 --controller-type f
```

![](./images/controller.gif)


<img src="./images/controller_force.png" width="600">

<img src="./images/controller.png">


### Controller. Impulse response

Now we add an impulse disturbance force to check how the controller is performing:

```
./sim_pendulum -n 200 --controller-type f --signal-type i --t0 50 --force-amplitude 100
```

![](./images/controller_impulse.gif)


<img src="./images/controller_impulse_force.png" width="600">

<img src="./images/controller_impulse.png">



### Controller. Cart position set-point

We can also set the cart position setpoint to specify where do we want to move the cart:

```
./sim_pendulum -n 200 --controller-type f --signal-type i --t0 50 --force-amplitude 100 --R1 2.0
```

![](./images/controller_impulse_setpoint.gif)

<img src="./images/controller_impulse_setpoint_force.png" width="600">

<img src="./images/controller_impulse_setpoint.png">
