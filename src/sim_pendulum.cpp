// Copyright 2019 Carlos San Vicente
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <getopt.h>

#include <iostream>
#include <vector>
#include <cmath>
#include <random>
#include <fstream>
#include <memory>

#include "runge_kutta.hpp"
#include "test_signal.hpp"
#include "controller.hpp"

static const double PI = 3.14159265359;

// inverted pendulum parameters
static double g = -9.8;  // gravity
static double m = 1.0;   // pendulum mass
static double M = 5.0;   // cart mass
static double L = 2.0;   // pendulum length
static double d = 20.0;  // cart damping coefficient

std::random_device rd;  // Will be used to obtain a seed for the random number engine
std::mt19937 gen(rd());  // Standard mersenne_twister_engine seeded with rd()
std::uniform_real_distribution<> dis(-0.01, 0.01);

const char* const short_opts = "x:v:t:w:n:bs:w:hm:M:L:l:d:c:1:2:3:4:5:6:7:8:";
const option long_opts[] = {
        {"cart-pos", required_argument, nullptr, 'x'},
        {"cart-vel", required_argument, nullptr, 'v'},
        {"pend-pos", required_argument, nullptr, 't'},
        {"pend-vel", required_argument, nullptr, 'w'},
        {"step", required_argument, nullptr, 's'},
        {"num-steps", required_argument, nullptr, 'n'},
        {"pend-mass", required_argument, nullptr, 'm'},
        {"cart-mass", required_argument, nullptr, 'M'},
        {"pend-length", required_argument, nullptr, 'L'},
        {"damp", required_argument, nullptr, 'd'},
        {"force-amplitude", required_argument, nullptr, 'F'},
        {"signal-type", required_argument, nullptr, 'S'},
        {"controller-type", required_argument, nullptr, 'c'},
        {"t0", required_argument, nullptr, 'Z'},
        {"t1", required_argument, nullptr, 'X'},
        {"K1", required_argument, nullptr, '1'},
        {"K2", required_argument, nullptr, '2'},
        {"K3", required_argument, nullptr, '3'},
        {"K4", required_argument, nullptr, '4'},
        {"R1", required_argument, nullptr, '5'},
        {"R2", required_argument, nullptr, '6'},
        {"R3", required_argument, nullptr, '7'},
        {"R4", required_argument, nullptr, '8'},
        {"file", required_argument, nullptr, 'f'},
        {"verbose", no_argument, nullptr, 'V'},
        {"help", no_argument, nullptr, 'h'},
        {nullptr, no_argument, nullptr, 0}
};

// state vector "y" or "state"
// state[0] -> cart position
// state[1] -> cart velocity
// state[2] -> pendulum angle
// state[3] -> pendulum angular velocity

// Derivative function to integrate the pendulum ODE equations
double invertedPendulum(const std::vector<double> &y, double u, size_t i)
{
  if (i == 0) {
    return y[1];
  } else if (i == 1) {
    double Sy = sin(y[2]);
    double Cy = cos(y[2]);
    double D = m*L*L*(M+m*(1-Cy*Cy));
    return (1/D)*(-m*m*L*L*g*Cy*Sy + m*L*L*(m*L*y[3]*y[3]*Sy - d*y[1])) + m*L*L*(1/D)*u;
  } else if (i == 2) {
    return y[3];
  } else if (i == 3) {
    double Sy = sin(y[2]);
    double Cy = cos(y[2]);
    double D = m*L*L*(M+m*(1-Cy*Cy));
    return (1/D)*((m+M)*m*g*L*Sy - m*L*Cy*(m*L*y[3]*y[3]*Sy - d*y[1])) - m*L*Cy*(1/D)*u +dis(gen);
  } else {
      throw std::invalid_argument("received wrong index");
  }
}

void PrintHelp()
{
    std::cout <<
      "help:\n"
            "\t--cart-pos:          Set initial cart position\n"
            "\t--cart-vel:          Set initial cart velocity\n"
            "\t--pend-pos:          Set initial pendulum position\n"
            "\t--pend-vel:          Set initial pendulum velocity\n"
            "\t--step:              Set time step size\n"
            "\t--num-step:          Set mumber of steps\n"
            "\t--pend-mass:         Set pendulum mass (Kg)\n"
            "\t--cart-mass:         Set cart mass (Kg)\n"
            "\t--pend-length:       Set pendulum length\n"
            "\t--damp:              Set cart damping coefficient\n"
            "\t--force-amplitude:   Set force perturbance signal amplitude\n"
            "\t--signal-type:       Set force perturbance signal type\n"
            "\t                     n: none, i: impulse (t0), s: step (t0)\n"
            "\t                     r: ramp (t0, t1), q: square (t0, t1)\n"
            "\t--t0:                Set force perturbance signal t0 (start time)\n"
            "\t--t1:                Set force perturbance signal t1 (step, ramp, square)\n"
            "\t--controller-type:   Set controller type\n"
            "\t                     n: none (default), f: full state feedback (K1,K2,K3,K4)\n"
            "\t--K1:                Set feedback matrix K gain K1\n"
            "\t--K2:                Set feedback matrix K gain K2\n"
            "\t--K3:                Set feedback matrix K gain K3\n"
            "\t--K4:                Set feedback matrix K gain K4\n"
            "\t--R1:                Set reference value. Cart position\n"
            "\t--R2:                Set reference value. Cart velocity\n"
            "\t--R3:                Set reference value. Pendulum angle\n"
            "\t--R4:                Set reference value. Pendulum velocity\n"
            "\t--file <fname>:      File to write to\n"
            "\t--verbose            Print data to the standard output\n"
            "\t--help:              Show help\n";
}


int main(int argc, char **argv)
{
  std::vector<double> state{0.0, 0.0, PI, 0.0};
  std::string file_name = "./simulations/sim_data.txt";
  std::ofstream sim_file;
  RungeKutta integrator(state.size());
  int num_steps = 201;
  double step = 0.1;
  double cart_force_x = 0.0;
  double t = 0.0;
  double force_amplitude = 0.0;
  std::unique_ptr<TestSignal> external_force;
  char signal_option = 'n';
  int t0 = 0.0;
  int t1 = 0.0;
  bool verbose = false;
  char controller_option = 'n';
  std::unique_ptr<IController> controller;
  std::vector<double> K = {-10.0000, -51.5393, 356.8637, 154.4146};
  std::vector<double> R = {0.0, 0.0, PI, 0.0};

  // process args
  while (true)
  {
     int option_index = 0;
     const auto opt = getopt_long(argc, argv, short_opts, long_opts, &option_index);

     if (-1 == opt)
         break;

     switch (opt)
     {
     case 's':
         step = std::stod(optarg);
         break;
     case 'n':
         num_steps = std::stoi(optarg);
         break;
     case 'x':
         state[0] = std::stod(optarg);
         break;
     case 'v':
         state[1] = std::stod(optarg);
         break;
     case 't':
         state[2] = std::stod(optarg);
         break;
     case 'w':
         state[3] = std::stod(optarg);
         break;
     case 'm':
         m = std::stod(optarg);
         break;
     case 'M':
         M = std::stod(optarg);
         break;
     case 'l':
     case 'L':
         L = std::stod(optarg);
         break;
     case 'd':
         d = std::stod(optarg);
         break;
     case 'F':
         force_amplitude = std::stod(optarg);
         break;
     case 'S':
         signal_option = optarg[0];
         break;
     case 'Z':
          t0 = atoi(optarg);
         break;
     case 'X':
         t1 = atoi(optarg);
         break;
     case 'c':
         controller_option = optarg[0];
         break;
     case '1':
         K[0] = std::stod(optarg);
         break;
     case '2':
         K[1] = std::stod(optarg);
         break;
     case '3':
         K[2] = std::stod(optarg);
         break;
     case '4':
         K[3] = std::stod(optarg);
         break;
     case '5':
         R[0] = std::stod(optarg);
         break;
     case '6':
         R[1] = std::stod(optarg);
         break;
     case '7':
         R[2] = std::stod(optarg);
         break;
     case '8':
         R[3] = std::stod(optarg);
         break;
     case 'f':
         file_name = std::string(optarg);
         break;
     case 'V':
         verbose = true;
      break;
     case 'h':  // -h or --help
     case '?':  // Unrecognized option
     default:
         PrintHelp();
         return EXIT_FAILURE;
     }
  }

  std::cout << "Pendulum params:" << std::endl;
  std::cout << "  Pendulum length: " << L << std::endl;
  std::cout << "  Cart mass: " << M << std::endl;
  std::cout << "  Pendulum mass: " << m << std::endl;
  std::cout << "  Cart damping coefficient: " << d << std::endl;

  std::cout << "Initial state:" << std::endl;
  std::cout << "  Cart initial position: " << state[0] << std::endl;
  std::cout << "  Cart initial velocity: " << state[1] << std::endl;
  std::cout << "  Pendulum initial velocity: " << state[3] << std::endl;
  std::cout << "  Pendulum initial position: " << state[2] << std::endl;

  std::cout << "Simulation params:" << std::endl;
  std::cout << "  Time step: " << step << std::endl;
  std::cout << "  Number of steps: " << num_steps << std::endl;

  std::cout << "Write file set to: " << file_name << std::endl;

  std::cout << "Test signal params:" << std::endl;
  if (signal_option == 'i') {
    // TODO(carlosvg) check params
    std::cout << "  Type: impulse." << std::endl;
    std::cout << "  Amplitude = " << force_amplitude << std::endl;
    std::cout << "  t0 = "<< t0 << std::endl;
    external_force = std::make_unique<Impulse>(force_amplitude, t0);
  } else if (signal_option == 's') {
    std::cout << "  Type: step." << std::endl;
    std::cout << "  Amplitude = " << force_amplitude << std::endl;
    std::cout << "  t0 = "<< t0 << std::endl;
    external_force = std::make_unique<Step>(force_amplitude, t0);
  } else if (signal_option == 'r') {
    // TODO(carlosvg) check params
    std::cout << "  Type: step." << std::endl;
    std::cout << "  Amplitude = " << force_amplitude << std::endl;
    std::cout << "  t0 = " << t0 << ", t1 = " << t1 << std::endl;
    external_force = std::make_unique<Ramp>(force_amplitude, t0, t1);
  } else if (signal_option == 'q') {
    // TODO(carlosvg) check params
    std::cout << "  Type: square." << std::endl;
    std::cout << "  Amplitude = " << force_amplitude << std::endl;
    std::cout << "  t0 = "<< t0 << ", t1 = " << t1 << std::endl;
    external_force = std::make_unique<Square>(force_amplitude, t0, t1-t0);
  } else {
    std::cout << "  Type: none." << std::endl;
    external_force = std::make_unique<NoSignal>();
  }

  std::cout << "Controller params:" << std::endl;

  if (controller_option == 'f') {
    controller = std::make_unique<FullStateFeedbackController>(K);
    std::cout << "  Type: full state feedback." << std::endl;
    std::cout << "  K: [";
    for (auto k : K) { std::cout << " " << k; }
    std::cout << "]" << std::endl;
    std::cout << "  R: [";
    for (auto r : R) { std::cout << " " << r; }
    std::cout << "]" << std::endl;
  } else {
    controller = std::make_unique<NoController>();
  }

  sim_file.open(file_name.c_str());
  if (!sim_file.is_open())
  {
    std::cout << "Error opening file";
    sim_file.close();
    return EXIT_FAILURE;
  }

  for (int i = 0; i < num_steps; i++) {
    double controller_force = controller->calculate(state, R);
    double disturbance_force = external_force->sample(i);
    cart_force_x = disturbance_force + controller_force;
    integrator.step(invertedPendulum, state, step, cart_force_x);

    if (verbose) {
      std::cout << t << " ";
      std::cout << state[0] << " ";  // cart position
      std::cout << state[1] << " ";  // cart velocity
      std::cout << state[2] << " ";  // pendulum angle
      std::cout << state[3] << " ";  // pendulum angular velocity
      std::cout << cart_force_x << std::endl;  // pendulum angular velocity
    }

    sim_file << t << " ";
    sim_file << state[0] << " ";  // cart position
    sim_file << state[1] << " ";  // cart velocity
    sim_file << state[2] << " ";  // pendulum angle
    sim_file << state[3] << " ";  // pendulum angular velocity
    sim_file << cart_force_x << std::endl;  // pendulum angular velocity
    t += step;
  }

  sim_file.close();
  return EXIT_SUCCESS;
}
