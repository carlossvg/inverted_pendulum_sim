// Copyright 2019 Carlos San Vicente
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TESTSIGNAL__TEST_SIGNAL_HPP_
#define TESTSIGNAL__TEST_SIGNAL_HPP_

class TestSignal
{
public:
  virtual double sample(int t) = 0;
  virtual ~TestSignal() {}
};

class NoSignal : public TestSignal
{
public:
  double sample(int ) override { return 0.0; }
};


class Impulse : public TestSignal
{
public:
  Impulse(double A, int t0)
  : amplitude(A), t0(t0) {}
  virtual double sample(int t) override { return (t == t0) ? amplitude : 0.0; }
private:
  double amplitude;
  int t0;
};

class Step : public TestSignal
{
public:
  Step(double A, int t0)
  : amplitude(A), t0(t0) {}
  virtual double sample(int t) override { return (t >= t0) ? amplitude : 0.0; }
private:
  double amplitude;
  int t0;
};

class Ramp : public TestSignal
{
public:
  Ramp(double A, int t0, int t1)
  : amplitude(A), t0(t0), t1(t1) {}
  virtual double sample(int t) override {
    if (t >= t0 && t <= t1) {
      current += amplitude/(t1-t0);
    }
    return current;
  }
private:
  double amplitude;
  int t0, t1;
  double current = 0.0;
};

class Square : public TestSignal
{
public:
  Square(double A, int t0, int width)
  : amplitude(A), t0(t0), width(width) {}
  virtual double sample(int t) override {
    if (t < t0 ) { return 0.0; }
    int elapsed_steps = t-t0;
    if ( (elapsed_steps % width) == 0.0 ) {
      sign = -sign;
    }
    return sign * amplitude;
  }
private:
  double amplitude;
  int t0;
  int width;
  int sign = -1;
};

#endif  // TESTSIGNAL__TEST_SIGNAL_HPP_
