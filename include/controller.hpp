// Copyright 2019 Carlos San Vicente
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef _FULL_STATE_FEEDBACK_CONTROLLER_H_
#define _FULL_STATE_FEEDBACK_CONTROLLER_H_

#include <vector>
#include <stdexcept>

class IController
{
public:
  virtual double calculate(const std::vector<double> & state_vector,
    const std::vector<double> & reference_vector) const = 0;
  virtual ~IController() {}
};

class NoController : public IController
{
public:

  double calculate(const std::vector<double> & state,
    const std::vector<double> & reference) const override { return 0.0; }
};

// Implements a Full State Feedback controller
// https://en.wikipedia.org/wiki/Full_state_feedback
class FullStateFeedbackController : public IController
{
public:
  FullStateFeedbackController(const std::vector<double> & feedback_matrix)
  : feedback_matrix_(feedback_matrix) { }

  double calculate(const std::vector<double> & state,
    const std::vector<double> & reference) const override
    {
      double controller_output = 0.0;
      size_t dim = state.size();
      if ( (dim != reference.size()) &&
           (dim != feedback_matrix_.size()) ) {
        throw std::invalid_argument("wrong state size vector");
      }

      for (size_t i = 0; i < dim; i++) {
        controller_output += -feedback_matrix_[i]*(state[i]-reference[i]);
      }

      return controller_output;
    }
private:
  std::vector<double> feedback_matrix_;
};


#endif // _FULL_STATE_FEEDBACK_CONTROLLER_H_
